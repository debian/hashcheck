/*******************************************************************************
 * Copyright (c) 2017, Kyle Robbertze (AIMS, South Africa)
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Main GUI widget
 * 
 *        Created:  23/02/2017 11:41:31
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), kyle@aims.ac.za
 *******************************************************************************/
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QString>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    public:
        MainWindow(QWidget* parent = nullptr);
    private slots:
        void onVerifyClicked();
        void onAboutTriggered();
    private:
        Ui::MainWindow* ui;
        QString m_root { "/lib/live/mount/medium" };
        void appendStatus(QString text);
};

#endif // MAIN_WINDOW_H
